const productModel = require('../models/product.model');

const pathUtil = require('../utils/path')



const getAddProductTemplate = (req, res, next) => {
    res.sendFile(pathUtil.path.join(pathUtil.rootDir, 'templates', 'add-product.html'));
}
const postAddProduct = (req, res, next) => {
    const product = new productModel.Product(req.body.title);
    product.save();
    res.redirect('/');
}
const getProducts = (req, res, next) => {
    productModel.Product.fetchAll((products)=>{
        console.log(products);
    })
    res.sendFile(pathUtil.path.join(pathUtil.rootDir,'templates','shop.html'));
}



module.exports = {
    getAddProductTemplate: getAddProductTemplate,
    postAddProduct: postAddProduct,
    getProducts, getProducts
};
