const {validationResult} = require('express-validator/check');


exports.getPosts = (req, res, next) => {
   return  res.status(200).json({
       posts: [{
           _id: '1',
           title: 'Sample Post',
           content: 'This is Sample Post!',
           imageUrl: 'images/duck.jpg',
           creator: {
               name: 'Max'
           },
           createdAt: new Date()
       }]
   });
};

exports.createPost = (req, res, next) => {
    const errors = validationResult(req);
    if(!error.isEmpty()){
        return res.status(422).json({message: 'Entered Data Incorrect or did not meet requirements!', errors: errors});
    }
    const title = req.body.title;
    const content = req.body.content;
    return  res.status(201).json({
        message: 'Post Created Successfully!',
        post: {
            _id: '2',
            title: title,
            content: content,
        imageUrl: 'images/duck.jpeg',
        creator: {
            name: 'Max'
        },
        createdAt: new Date()
    }
    });
};
