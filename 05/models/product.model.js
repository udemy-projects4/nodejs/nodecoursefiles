

const pathUtil = require('../utils/path');
const fs = require("fs");

const productsJSON = pathUtil.path.join(pathUtil.rootDir, 'data', 'products.json');


class Product {
    constructor(t) {
        this.title = t;
    }

    save() {

        fs.readFile(productsJSON, (err, fileContent) => {
          let products = [];
          if (!err){
              products = JSON.parse(fileContent);
          }
          products.push(this);
          fs.writeFile(productsJSON, JSON.stringify(products), (err) => {

          });
        });
    }

    static fetchAll(cb) {
        let products = [];
         fs.readFile(productsJSON, (err, fileContent) => {
            if (!err) {
                products = JSON.parse(fileContent);
            }
            return cb(products);
        });

    }
}


module.exports = {
    Product: Product
}
