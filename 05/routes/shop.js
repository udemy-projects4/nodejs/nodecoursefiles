const express = require('express');

const productsView = require('../views/products.view');


const router = express.Router();

router.get('/', productsView.getProducts);

module.exports = router
