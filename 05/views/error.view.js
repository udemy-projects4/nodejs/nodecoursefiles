

const pathUtil = require('../utils/path');


const get404Template = (req, res, next) => {
    res.status(404).sendFile(pathUtil.path.join(pathUtil.rootDir,'templates','404.html'))
}

module.exports = {
    get404Template: get404Template,

}
