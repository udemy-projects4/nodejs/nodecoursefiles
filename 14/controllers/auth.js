exports.getLogin = (req, res, next) => {
  res.render('auth/login', {
    path: '/login',
    pageTitle: 'Login',
    isAuthenticated: req.get('Cookie').split(';')[1].trim().split('=')[1]
  });
};

exports.postLogin = (req, res,next) => {
  req.session.isLoggedIn = true;
  res.redirect('/');
};
