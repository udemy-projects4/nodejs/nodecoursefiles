const path = require('path');


module.exports = {
    path: path,
    rootDir: path.dirname(process.mainModule.filename)
};
