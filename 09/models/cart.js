const fs = require('fs');
const pathUtil = require('../util/path');

const p = pathUtil.path.join(
    pathUtil.rootDir,
    'data',
    'cart.json'
);

module.exports = class Cart {

    static addProduct(id, productPrice) {
        fs.readFile(p, (err, fileContent) => {
           let cart = { products: [], total: 0, totalPrice: 0};
           if(!err){
               cart = JSON.parse(fileContent);
           }

           const existingProductIndex = cart.products.findIndex(prod => prod.id === id);
           const existingProduct = cart.products[existingProductIndex];
           let updatedProduct;
           if(existingProduct) {
               updatedProduct = { ...existingProduct};
               updatedProduct.qty++;
               cart.products[existingProductIndex] = updatedProduct;
           }
           else{
               updatedProduct = {id: id, qty: 1};
               cart.products = [...cart.products, updatedProduct];
           }
           cart.total++;
           cart.totalPrice = cart.totalPrice + +productPrice;
            fs.writeFile(p, JSON.stringify(cart), err => {
                console.log(err);
            });
        });
    }
}
