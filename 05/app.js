const pathUtil = require('./utils/path');
const express = require('express');
const bodyParser = require('body-parser');

const errorsView = require('./views/error.view');

const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');

const port= 8080;

const app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(pathUtil.path.join(pathUtil.rootDir, 'public')))

app.use('/admin',adminRoutes);
app.use(shopRoutes);

app.use(errorsView.get404Template);

app.listen(port);
