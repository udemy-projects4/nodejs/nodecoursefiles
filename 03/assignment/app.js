const http = require('http');
const htmlGen = require('./htmlGen');

const port = 3000;


const server = http.createServer((req, res) => {
    const url = req.url;
    const method = req.method;
    console.log(method," ",url);

    if(url === '/'){
        let responseData = htmlGen.generate('Node Assignment','<h1>Welcome to Assignment Server</h1><form action="/create-user" method="post"><input type="text" name="username" placeholder="username"><button type="submit">Submit</button> </form>');
        res.write(responseData);
        return res.end();
    }
    else if(url === '/users'){
        let responseData = htmlGen.generate('NA - Users', '<ul><li>Lisa</li><li>Luciana</li><li>Vivin</li><li>Zud</li></ul>');
        res.write(responseData);
        return res.end();
    }
    else if(url ==='/create-user'){
        if(method === 'POST'){
            const body = [];
            req.on('data', (chunck) =>{
                body.push(chunck);
            });
            req.on('end',() => {
                let requestData = Buffer.concat(body).toString();
                let username = requestData.split('=')[1];
                console.log("New User: ",username," Created!")
                let responseData = htmlGen.generate('NA - Create User', `<h1>New User: ${username} Created!</h1>`);
                res.write(responseData);
                return res.end();
            });
        }
        else{
            console.error("405 ", method," on ",url, "not allowed");
            let responseData = htmlGen.generate('Not Allowed!', `<h1>Internal Server Error: 405 Method not allowed!</h1><h3>${method} on ${url} not allowed</h3>`);
            res.write(responseData);
            return res.end();
        }
    }

    else{
        console.error("404 ",url," Not found")
        let responseData = htmlGen.generate('Not Found!', `<h1>Internal Server Error: 404 Page not found!</h1><h3>${url} Not found on the server.</h3>`);
        res.write(responseData);
        return res.end();
    }
});

console.log('Server Starting...');
server.listen(port);
console.log('Server listening on', port);
