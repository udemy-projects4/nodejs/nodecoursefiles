const express = require('express');
const bodyParser = require('body-parser');

const feedRoutes = require('./routes/feed.route');

const PORT = 8080;

const app = express();

app.use(bodyParser.json());
app.use((req,res,next) => {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.setHeader('Access-Contol-Allow-Methods', 'GET, POST, PUT, PATCH, OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});

app.use('/feed', feedRoutes);



app.listen(PORT);
