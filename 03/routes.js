const fs = require('fs');


const requestHandler = (req, res) =>{
    const url = req.url;
    const method = req.method;
    if(url === '/'){
        res.setHeader('Content-Type', 'text/html');
        res.write('<html>');
        res.write('<head><title>Enter a Message</title></head>');
        res.write('<body><form action="/message" method="post"><input type="text" name="message"><button type="submit">Submit</button> </form>')
        res.write('</html>');
        return res.end();
    }
    if(url === '/message' && method === 'POST'){
        const body = [];
        req.on('data', (dataChunk) =>{
            body.push(dataChunk);
        });
        return req.on('end', () =>{
            const parsedBody = Buffer.concat(body).toString();
            const message = parsedBody.split('=')[1];
            fs.writeFile('message.txt', message, err => {
                res.writeHead(302, {
                    'Location': '/'
                });
                return res.end()
            });

        });

    }
    console.log("RequestListener:", req, res);
}

module.exports = {
    handler: requestHandler
};
