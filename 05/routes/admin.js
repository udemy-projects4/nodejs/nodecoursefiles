const express = require('express');


const productsView = require('../views/products.view');


const router = express.Router();

router.get('/add-product', productsView.getAddProductTemplate);

router.post('/add-product', productsView.postAddProduct);

module.exports = router
