const express = require('express');
const {body} = require('express-validator/check');

const feedView = require('../views/feed.view');

const router = express.Router();

router.get('/posts', feedView.getPosts);
router.post('/posts',
    [
        body('title').trim().isLength({min: 5}),
        body('content').trim().isLength({min: 5})
    ],
    feedView.createPost);

module.exports = router;
